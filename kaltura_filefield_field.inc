<?php

/**
 * @file
 *
 * - Kaltura Filefiled Field Settings Overrides
 */

/**
 * Implementation of hook_field_settings_alter().
 */
function kaltura_filefield_field_settings_alter(&$settings, $op, $field) {
  if ($field['type'] == 'filefield') {
    switch ($op) {
      case 'form':
        $kaltura_filefield_sync = ($field['kaltura_filefield_sync']) ? $field['kaltura_filefield_sync'] : NULL;
        $settings['kaltura_filefield_sync'] = array(
          '#type' => 'checkbox',
          '#title' => t('Kaltura: Sync Filefield to the Kaltura Sever'),
          '#description' => t('Sync file data with the Kaltura Media Server.'),
          '#default_value' => $kaltura_filefield_sync,
          '#required' => FALSE,
          '#size' => '15',
          '#weight' => 50,
          );    
        $player_id = ($field['kaltura_player_id']) ? $field['kaltura_player_id'] : NULL;
        $settings['kaltura_player_id'] = array(
          '#type' => 'textfield',
          '#title' => t('Kaltura: Player ID'),
          '#default_value' => $player_id,
          '#required' => FALSE,
          '#size' => '15',
          '#weight' => 51,
          '#element_validate' => array('_element_validate_integer_positive'),          
          );
        $kaltura_use_thumbnail = ($field['kaltura_use_thumbnail']) ? $field['kaltura_use_thumbnail'] : NULL;
        $settings['kaltura_use_thumbnail'] = array(
          '#type' => 'checkbox',
          '#title' => t('Kaltura: Embed a Clickable Player Thumbnail'),
          '#description' => t('Insert a thumbnail that is replaced by a Kaltura player when clicked 
            (useful when you have multiple players on a single page). You may override the thumbnail in your css.'),
          '#default_value' => $kaltura_use_thumbnail,
          '#required' => FALSE,
          '#size' => '15',
          '#weight' => 52,
          );
        $kaltura_filefield_tags = ($field['kaltura_filefield_tags']) ? $field['kaltura_filefield_tags'] : NULL;       
        $settings['kaltura_filefield_tags'] = array(
          '#type' => 'textfield',
          '#title' => t('Kaltura: Override Global Tags For This Field'),
          '#size' => 50,
          '#weight' => 53,
          '#required' => FALSE,
          '#description' => t('Apply a set of Kaltura tags to every submitted entry (separated by commas) for this individual field.'),
          '#default_value' => $kaltura_filefield_tags,
          );
        $kaltura_filefield_categories = ($field['kaltura_filefield_categories']) ? $field['kaltura_filefield_categories'] : NULL;       
        $settings['kaltura_filefield_categories'] = array(
          '#type' => 'textfield',
          '#title' => t('Kaltura: Override Global Categories For This Field'),
          '#size' => 50,
          '#weight' => 54,
          '#required' => FALSE,
          '#description' => t('Apply a set of Kaltura categories to every submitted entry (separated by commas) for this individual field.'),
          '#default_value' => $kaltura_filefield_categories,
          );        
        $kaltura_filefield_access_id = ($field['kaltura_filefield_access_id']) ? $field['kaltura_filefield_access_id'] : NULL;       
        $settings['kaltura_filefield_access_id'] = array(
          '#type' => 'textfield',
          '#title' => t('Kaltura: Override Global Access Control Profile ID For This Field'),
          '#size' => 10,
          '#required' => FALSE,
          '#description' => t('Override the Kaltura Filefield Global settings and set a field-specific Kaltura Access Control Profile ID. Find the access control ID  in the Kaltura dashboard (kmc.kaltura.com) at Settings > Access Control. Uses "default" profile if not set here.'),
          '#default_value' => $kaltura_filefield_access_id,
          '#weight' => 55,
          );
        $kaltura_transcoding_profile_id = ($field['kaltura_transcoding_profile_id']) ? $field['kaltura_transcoding_profile_id'] : NULL;       
        $settings['kaltura_transcoding_profile_id'] = array(
          '#type' => 'textfield',
          '#title' => t('Kaltura: Override Global Transcoding Profile ID For This Field'),
          '#size' => 10,
          '#required' => FALSE,
          '#weight' => 56,          
          '#description' => t('Find the profile and its ID in the Kaltura dashboard (kmc.kaltura.com) at Settings > Transcoding Settings. Uses "default" profile if not set here.'),
          '#default_value' => $kaltura_transcoding_profile_id,
         );        
        break;
      case 'save':
        $settings[] = 'kaltura_player_id';
        $settings[] = 'kaltura_use_thumbnail';
        $settings[] = 'kaltura_filefield_sync';
        $settings[] = 'kaltura_filefield_tags';
        $settings[] = 'kaltura_filefield_categories';
        $settings[] = 'kaltura_filefield_access_id';
        $settings[] = 'kaltura_transcoding_profile_id';
        break;
    }
  }
}

/**
* Implementation of hook_theme().
*/
function kaltura_filefield_theme() {
  return array(
    // Themes for the formatters.
    'kaltura_filefield_formatter_kaltura_player' => array(
      'arguments' => array('element' => NULL),
    ),   
  );
}

/**
* Implementation of hook_field_formatter_info().
* 
*/
function kaltura_filefield_field_formatter_info() {
  return array(
    'kaltura_player' => array(
      'label' => t('Kaltura Filefield Player'),
      'field types' =>  array('filefield'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
  );
}

/**
* Theme function for Kaltura Filefield Player
* 
* Override this in your theme directory if you want to add your own js params
* or perform other customizations.
* 
* @param array $element
*  A cck field element array 
*/
function theme_kaltura_filefield_formatter_kaltura_player($element) {
  $target_id = md5($entry_id . time() . rand());
  $field_name = $element['#field_name'];
  $field = content_fields($field_name);
  if ($player_id = $field['kaltura_player_id']) {
    $config       = kaltura_filefield_global_config();
    $entry_id     = kaltura_filefield_entry_id_get($element['#item']['fid']);
    if (!$entry_id && $element['#item']['fid']) {
      return sprintf('<div class="kaltura-transcoding">%s</div>', t('Media Failed to Post to Kaltura, Please Re-upload The File.'));
    }
    elseif(!$entry_id) {
      return '<div class="kaltura-empty"></div>';
    }
    elseif (kaltura_filefield_local_status_get($entry_id) == 0) {
      return sprintf('<div class="kaltura-transcoding">%s</div>', t('Media Processing'));
    }
    $type         = $element['#type_name'];
    $partner_id   = $config['kaltura_partner_id'];
    $embed_method = ($field['kaltura_use_thumbnail']) ? 'kWidget.thumbEmbed' : 'kWidget.embed';
    $player = 
      "<div id=\"$target_id\" class=\"clearfix kaltura-player kaltura-player-$field_name kaltura-player-$type player-$player_id\"  itemprop=\"video\" itemscope itemtype=\"http://schema.org/VideoObject\">
      </div> 
      <script src=\"http://cdnapi.kaltura.com/p/$partner_id/sp/$partner_id/embedIframeJs/uiconf_id/$player_id/partner_id/$partner_id\"></script>
      <script>
            if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
              mw.setConfig(\"forceMobileHTML5\", true);
              kWidget.embed( '$target_id', { 
                'wid': '_$partner_id',
                'uiconf_id': '$player_id',
                'entry_id' : '$entry_id',
                'width' : '100',
                'height' : '30',
                'style' : 'border: 0px; max-width: 100%; max-height: 100%;'
              });
            }
            else {
              $embed_method({
                 'targetId': '$target_id',
                 'wid': '_$partner_id',
                 'uiconf_id': '$player_id',
                 'entry_id': '$entry_id',
                 'playBtn': '$thumb_id',
                 'flashvars':{ // flashvars allows you to set runtime uiVar configuration overrides. 
                      'autoPlay': false,
                      'streamerType': 'auto'
                 },
                 'params':{ // params allows you to set flash embed params such as wmode, allowFullScreen etc
                      'wmode': 'transparent'
                 },
              });
            }
      </script>";
    return $player;
  }
  else {
    return t('Player ID Missing from Field Settings');
  }
}